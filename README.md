# Getting Started

### Uruchomienie frontendu

W katalogu "frontend"
uruchamiany następujące komendy:
###
Wymagany node.js

npm install

npm run serve

W przypadku uruchomienia backendu na innym porcie niż 8080 należy zmienić scieżkę w pliku constants

ctrl+c w oknie terminalu aby zakończyć działanie 

### Baza Danych
Wykorzystana została baza PostgreSQL. 

Skrypt do wstawienia przykładowych danych znaduje się w pliku dbscripts.

Połączenie do bazy danych konfigurujemy w pliku hibernate.cfg.xml. Baza automatycznie utworzy tabele.

### Zrealizowane funkcjonalności
Dododwanie, usuwanie oraz edycja książek i użytkowników. (Edycja - kliknięcie na wiersz w tabeli na froncie)
Wypożyczanie książek użytkownikom oraz rejestrowanie zwrotu.

