package com.kl.library.borrow.repository;

import com.kl.library.models.Borrow.Borrow;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BorrowRepository extends CrudRepository<Borrow, Long> {

    List<Borrow> findAllByClientId(int id);

    List<Borrow> findAll();

    Borrow save(Borrow borrow);

    Borrow findBorrowByClientId(int id);

    Borrow findByBookId(int id);

    void deleteByBookId(int id);

    void deleteById(int id);


}
