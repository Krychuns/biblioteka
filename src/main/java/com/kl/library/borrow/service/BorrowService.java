package com.kl.library.borrow.service;

import com.kl.library.book.mapper.BookRepository;
import com.kl.library.borrow.repository.BorrowRepository;
import com.kl.library.models.Borrow.Borrow;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
@Transactional
@Service("borrowService")
public class BorrowService {

    private EntityManager em;

    private BorrowRepository br;

    private BookRepository bookr;

    public BorrowService(EntityManager em, BorrowRepository br, BookRepository bookr) {
        this.em = em;
        this.br = br;
        this.bookr = bookr;
    }

    public List<Borrow> findAllByClientId(int id) {
        return br.findAllByClientId(id);
    }

    public List<Borrow> findAll() {
        return br.findAll();
    }

    public void deleteByBookId(int id) {
        br.deleteByBookId(id);
    }

    public void deleteById(int id){
        br.deleteById(id);
    }



    public void create(Borrow borrow){
        if (bookr.findById(borrow.getBookId()).isStatus())
             br.save(borrow);
    }

    public Borrow update(Borrow borrow){
        return br.save(borrow);
    }


}