package com.kl.library.borrow.controller;


import com.kl.library.borrow.service.BorrowService;
import com.kl.library.models.Borrow.Borrow;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "borrows")
public class BorrowController {


    private BorrowService borrowService;


    public BorrowController(BorrowService borrowService) {
        this.borrowService = borrowService;
    }

    @PostMapping(value = "/create")
    public void create(@RequestBody Borrow borrow){
        borrowService.create(borrow);
    }

    @GetMapping(value = "/all")
    public List<Borrow> getAllBorrows() {
        return borrowService.findAll();
    }

    @GetMapping(value = "/userId/{userId}")
    public List<Borrow> getAllBorrowsByStatus(@PathVariable int userId) {
        return borrowService.findAllByClientId(userId);
    }

    @DeleteMapping(value = "/removeByBookId/{id}")
    public ResponseEntity deleteByBookId(@PathVariable int id){
        borrowService.deleteByBookId(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity deleteById(@PathVariable int id){
        borrowService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PatchMapping (value = "/update")
    public Borrow update(@RequestBody Borrow borrow) {return borrowService.update(borrow);}

}
