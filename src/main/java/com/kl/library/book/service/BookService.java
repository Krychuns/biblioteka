package com.kl.library.book.service;

import com.kl.library.book.mapper.BookRepository;
import com.kl.library.borrow.repository.BorrowRepository;
import com.kl.library.models.Book.Book;
import com.kl.library.models.Book.BookCategory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
@Transactional
@Service("bookService")
public class BookService  {

    private EntityManager em;

    private BookRepository bookRepository;

    private BorrowRepository borrowRepository;

    public BookService(EntityManager em, BookRepository bookRepository, BorrowRepository borrowRepository) {
        this.em = em;
        this.bookRepository = bookRepository;
        this.borrowRepository = borrowRepository;
    }

    public Book findById(int id) {
      return bookRepository.findById(id);
    }

    public List<Book> findAll(){
        return bookRepository.findAll();
    }

    public Book create(Book book){
        return bookRepository.save(book);
    }

    public List<Book> findAllByStatus(boolean status){
        return bookRepository.findAllByStatus(status);
    }

    public void deleteById(int id){
        if(borrowRepository.findByBookId(id)==null)
            bookRepository.deleteById(id);
    }

    public  Book update(Book book){
        return bookRepository.save(book);
    }


    public List<Enum> getCategories(){
        List<Enum> enumValues = new ArrayList<Enum>(EnumSet.allOf(BookCategory.class));
        return enumValues;
    }

}
