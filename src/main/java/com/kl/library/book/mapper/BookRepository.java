package com.kl.library.book.mapper;

import com.kl.library.models.Book.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository
        extends CrudRepository<Book, Long> {

    Book findById(int id);

    List<Book> findAll();

    List<Book> findAllByStatus(boolean status);

    void deleteById(int id);

    Book save(Book book);


}
