package com.kl.library.book.controller;

import com.kl.library.book.mapper.BookRepository;
import com.kl.library.book.service.BookService;
import com.kl.library.models.Book.Book;
import com.kl.library.models.Book.BookCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "books")
public class BookController {


    private BookService bookService;


    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping(value = "/categories")
    public List<Enum> getCategories() {
        return bookService.getCategories();
    }

    @GetMapping(value = "/id/{id}")
    public Book getById(@PathVariable int id) {
        return bookService.findById(id);
    }

    @GetMapping(value = "/all")
    public List<Book> getAllBooks() {
        return bookService.findAll();
    }

    @PostMapping(value = "/create")
    public Book create(@RequestBody Book book){
       return bookService.create(book);
    }

    @GetMapping(value = "/status/{status}")
    public List<Book> getAllBooksByStatus(@PathVariable boolean status) {
        return bookService.findAllByStatus(status);
    }

    @DeleteMapping(value = "remove/{id}")
    public ResponseEntity deleteById(@PathVariable int id){
        bookService.deleteById(id);
                return ResponseEntity.ok().build();
    }

    @PatchMapping (value = "/update")
    public Book update(@RequestBody Book book) {return bookService.update(book);}

}
