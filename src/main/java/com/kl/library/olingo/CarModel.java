package com.kl.library.olingo;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="car_model")
public class CarModel {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Integer year;

    @NotNull
    private String sku;

    @ManyToOne(optional=false,fetch=FetchType.LAZY) @JoinColumn(name="maker_fk")
    private CarMaker maker;

    // ... getters, setters and hashcode omitted
}
