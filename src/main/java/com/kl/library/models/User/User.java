package com.kl.library.models.User;

import javax.persistence.*;
@Entity
@Table(name ="Users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"id", "pesel"})
})
public class User {



    @Id @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "pesel", length = 11)
    private String pesel;

    @Column(name = "email")
    private String email;

    public User(){};

    public User(String name, String surname, String pesel, String email, String password) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
