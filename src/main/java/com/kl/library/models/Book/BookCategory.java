package com.kl.library.models.Book;

public enum BookCategory {
    THRILLER, TRAVEL, SCIFI, COOKING, HORROR, FANTASY, ROMANCE, BIOGRAPHY, HISTORY
}
