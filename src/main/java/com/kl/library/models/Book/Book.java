package com.kl.library.models.Book;

import javax.persistence.*;

@Entity
@Table(name ="Books", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"id", "libraryBookId"})
})
public class Book {



    @Id @GeneratedValue
    @Column(name ="id")
    private int id;

    @Column(name = "author")
    private String author;

    @Column(name = "title")
    private String title;

    @Column(name = "category")
    @Enumerated(EnumType.STRING)
    private BookCategory category;

    @Column(name = "authorisationDate", length = 4)
    private String authorisationDate;

    @Column(name = "libraryBookId")
    private int libraryBookId;

    @Column(name = "status")
    private boolean status;


    public Book(){}



    public Book( String author, String title, BookCategory category, String authorisationDate, int libraryBookId, boolean status) {
        this.author = author;
        this.title = title;
        this.category = category;
        this.authorisationDate = authorisationDate;
        this.status = status;
        this.libraryBookId = libraryBookId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BookCategory getCategory() {
        return category;
    }

    public void setCategory(BookCategory category) {
        this.category = category;
    }

    public String getAuthorisationDate() {
        return authorisationDate;
    }

    public void setAuthorisationDate(String authorisationDate) {
        this.authorisationDate = authorisationDate;
    }

    public int getLibraryBookId() {
        return libraryBookId;
    }

    public void setLibraryBookId(int libraryBookId) {
        this.libraryBookId = libraryBookId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }






}
