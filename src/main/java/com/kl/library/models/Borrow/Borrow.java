package com.kl.library.models.Borrow;

import com.kl.library.models.Book.BookCategory;
import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.time.LocalDate;
@Entity
@Table(name ="Borrows", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"id"})
})

public class Borrow {

    @Id
    @GeneratedValue
    @Column(name ="id")
    private int id;

    @Column(name = "borrowDate", nullable = false)
    private String borrowDate;

    @Column(name = "clientId", nullable = false)
    private int clientId;

    @Column(name = "bookId", nullable = false)
    private int bookId;


    public Borrow(){}

    public Borrow(String borrowDate, int clientId, int bookId) {
        this.borrowDate = borrowDate;
        this.clientId = clientId;
        this.bookId = bookId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(String borrowDate) {
        this.borrowDate = borrowDate;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }
}
