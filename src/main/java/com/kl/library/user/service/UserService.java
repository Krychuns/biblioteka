package com.kl.library.user.service;

import com.kl.library.book.mapper.BookRepository;
import com.kl.library.borrow.repository.BorrowRepository;
import com.kl.library.models.Borrow.Borrow;
import com.kl.library.models.User.User;
import com.kl.library.user.mapper.UserRepository;
import org.springframework.stereotype.Service;


import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service("userService")
public class UserService {

        private EntityManager em;

        private UserRepository ur;

        private BorrowRepository borrowRepository;

        public UserService(EntityManager em, UserRepository ur, BorrowRepository borrowRepository) {
            this.em = em;
            this.ur = ur;
            this.borrowRepository = borrowRepository;
        }

        public User findById(int id) {
            return ur.findById(id);
        }

        public List<User> findAll(){
            return ur.findAll();
        }

         public void deleteById(int id){
             if(borrowRepository.findBorrowByClientId(id)==null)
            ur.deleteById(id);
        }

        public User create(User user){
            return ur.save(user);
        }

        public User update(User user){
            return ur.save(user);
        }


}
