package com.kl.library.user.mapper;

import com.kl.library.models.User.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository
        extends CrudRepository<User, Long> {

    User findById(int id);

    List<User> findAll();

    void deleteById(int id);
}