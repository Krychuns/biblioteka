package com.kl.library.user.controller;

import com.kl.library.models.User.User;
import com.kl.library.user.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "users")
public class UserController {

    private UserService userService;


    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/id/{id}")
    public User getById(@PathVariable int id) {
        return userService.findById(id);
    }

    @GetMapping(value = "/all")
    public List<User> getAllUsers() {
        return userService.findAll();
    }

    @PostMapping(value = "/create")
    public User create(@RequestBody User User){
        return userService.create(User);
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteById(@PathVariable int id){
        userService.deleteById(id);
    }

    @PatchMapping (value = "/update")
    public User update(@RequestBody User User) {return userService.update(User);}


}
