import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token:'',
    authorization: false
  },
  getters: {
    isAuthorized: state => {
      return state.authorization
    }},

  mutations: {
    updateToken (state, tokenValue){

      if(!tokenValue.empty)
      {
        state.token=tokenValue;
        state.authorization=true;
        return true;
      }
      else {
        return false;
      }
    }
  },
  actions: {

  }
})
